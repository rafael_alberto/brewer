CREATE TABLE estilos (
    id int8 NOT NULL,
    nome VARCHAR(50) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE cervejas (
    id int8 NOT NULL,
    sku VARCHAR(50) NOT NULL,
    nome VARCHAR(80) NOT NULL,
    descricao VARCHAR(500) NOT NULL,
    valor NUMERIC(10, 2) NOT NULL,
    teor_alcoolico NUMERIC(10, 2) NOT NULL,
    comissao NUMERIC(10, 2) NOT NULL,
    sabor VARCHAR(50) NOT NULL,
    origem VARCHAR(50) NOT NULL,
    id_estilo int8 NOT NULL,
    primary key (id)
);

ALTER TABLE cervejas
	ADD CONSTRAINT fk_cervejas_estilos 
    FOREIGN KEY (id_estilo) 
    REFERENCES estilos;
            
CREATE SEQUENCE seq_cervejas;

CREATE SEQUENCE seq_estilos;

INSERT INTO estilos VALUES (nextval('seq_estilos'), 'Amber Lager');
INSERT INTO estilos VALUES (nextval('seq_estilos'), 'Dark Lager');
INSERT INTO estilos VALUES (nextval('seq_estilos'), 'Pale Lager');
INSERT INTO estilos VALUES (nextval('seq_estilos'), 'Pilsner');