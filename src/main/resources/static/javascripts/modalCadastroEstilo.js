var Brewer = Brewer || {};

Brewer.CadastroEstilo = (function() {

	function CadastroEstilo() {
		this.modal = $('#modalCadastroEstilo');
		this.botaoSalvar = this.modal.find('.js-btn-salvar-estilo');
		this.form = this.modal.find('form');
		this.url = this.form.attr('action');
		this.nomeEstilo = $('#nomeEstilo');
		this.containerMensagemErro = $('.js-mensagem-estilo');
	}

	CadastroEstilo.prototype.iniciar = function() {
		this.form.on('submit', function(e) {
			e.preventDefault()
		});
		this.modal.on('shown.bs.modal', onModalShow.bind(this));
		this.modal.on('hide.bs.modal', onModalClose.bind(this));
		this.botaoSalvar.on('click', onBotaoSalvarClick.bind(this));
	}

	function onModalShow() {
		this.nomeEstilo.focus();
	}

	function onModalClose() {
		this.nomeEstilo.val('');
		this.containerMensagemErro.addClass('hidden');
		this.form.find('.form-group').removeClass('has-error');
	}

	function onBotaoSalvarClick() {
		var nomeEstilo = this.nomeEstilo.val().trim();
		$.ajax({
			url : this.url,
			method : 'POST',
			contentType : 'application/json',
			data : JSON.stringify({
				nome : nomeEstilo
			}),
			error : onErroSalvarEstilo.bind(this),
			success : onEstiloSalvo.bind(this)
		});
	}

	function onErroSalvarEstilo(obj) {
		var mensagemErro = obj.responseText;
		this.containerMensagemErro.removeClass('hidden');
		this.containerMensagemErro.html('<span>' + mensagemErro + '</span>');
		this.form.find('.form-group').addClass('has-error');
		this.nomeEstilo.focus();
	}

	function onEstiloSalvo(estilo) {
		var comboEstilo = $('#estilo');
		comboEstilo.append('<option value=' + estilo.id + '>' + estilo.nome + '</option>');
		comboEstilo.val(estilo.id);
		this.modal.modal('hide');
	}

	return CadastroEstilo;

}());

$(function() {
	var cadastroEstilo = new Brewer.CadastroEstilo();
	cadastroEstilo.iniciar();
});