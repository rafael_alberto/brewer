package br.com.brewer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.brewer.dao.EstiloDao;
import br.com.brewer.exception.RegistroJaCadastradoException;
import br.com.brewer.model.Estilo;

@Service
public class EstiloService {

	@Autowired
	private EstiloDao estiloDao;
	
	public List<Estilo> buscarTodos(){
		return estiloDao.findAll();
	}
	
	@Transactional
	public Estilo salvar(Estilo estilo){
		Optional<Estilo> estiloOptional = estiloDao.findByNomeIgnoreCase(estilo.getNome());
		if(estiloOptional.isPresent()){
			throw new RegistroJaCadastradoException("Nome do estilo já cadastrado!");
		}
		return estiloDao.saveAndFlush(estilo);
	}
}
