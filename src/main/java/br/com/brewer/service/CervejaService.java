package br.com.brewer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.brewer.dao.CervejaDao;
import br.com.brewer.model.Cerveja;

@Service
public class CervejaService {

	@Autowired
	private CervejaDao cervejaDao;
	
	@Transactional
	public void salvar(Cerveja cerveja){
		cervejaDao.save(cerveja);
	}
}
