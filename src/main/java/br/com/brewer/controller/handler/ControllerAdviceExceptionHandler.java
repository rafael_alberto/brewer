package br.com.brewer.controller.handler;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.brewer.exception.RegistroJaCadastradoException;

@ControllerAdvice
public class ControllerAdviceExceptionHandler {

	@ExceptionHandler(RegistroJaCadastradoException.class)
	public ResponseEntity<String> handleRegistroJaCadastradoException(RegistroJaCadastradoException e){
		return ResponseEntity.badRequest().body(e.getMessage());
	}
}
