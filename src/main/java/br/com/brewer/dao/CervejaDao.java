package br.com.brewer.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.brewer.model.Cerveja;

@Repository
public interface CervejaDao extends JpaRepository<Cerveja, Long> {
	
	public Optional<Cerveja> findBySkuIgnoreCase(String sku);
}
