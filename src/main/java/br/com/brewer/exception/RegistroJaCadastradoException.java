package br.com.brewer.exception;

public class RegistroJaCadastradoException extends RuntimeException {

	private static final long serialVersionUID = -7584274081489405845L;

	public RegistroJaCadastradoException(String mensagem){
		super(mensagem);
	}
	
}
