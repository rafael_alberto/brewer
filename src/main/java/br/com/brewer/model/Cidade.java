package br.com.brewer.model;

import org.hibernate.validator.constraints.NotBlank;

public class Cidade {

	@NotBlank(message = "Nome deve ser informado!")
	private String nome;
	
	@NotBlank(message = "Estado deve ser informado!")
	private String estado;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
