package br.com.brewer.model;

import org.hibernate.validator.constraints.NotBlank;

public class Cliente {

	@NotBlank(message = "CPF/CNPJ deve ser informado!")
	private String cpfCnpj;
	
	@NotBlank(message = "Nome deve ser informado!")
	private String nome;

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
