package br.com.brewer.model;

import org.hibernate.validator.constraints.NotBlank;

public class Usuario {

	@NotBlank(message = "Nome deve ser informado!")
	private String nome;
	
	@NotBlank(message = "E-mail deve ser informado!")
	private String email;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
